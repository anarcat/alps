FROM golang:1.18 AS builder
RUN apt update && apt install -y ca-certificates
WORKDIR /src
COPY go.mod go.sum ./
RUN go mod download && go mod verify
COPY . .
RUN CGO_ENABLED=0 go build -o alps -tags netgo -v -a -ldflags '-extldflags "-static"' cmd/alps/main.go

FROM scratch
COPY --from=builder /etc/ssl/certs/* /etc/ssl/certs/
COPY --from=builder /src/alps /
COPY --from=builder /src/themes /themes
COPY --from=builder /src/plugins /plugins
EXPOSE 1323
ENTRYPOINT ["/alps"]
